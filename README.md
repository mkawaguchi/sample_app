[![alt tag](https://circleci.com/gh/kwgch/sample_app.png?circle-token=7de96299e4cad436bc7171e8a4c659ddc4e59375)](https://circleci.com/gh/kwgch/sample_app)
[![Build Status](https://travis-ci.org/kwgch/sample_app.png)](https://travis-ci.org/kwgch/sample_app)
[![Coverage Status](https://coveralls.io/repos/kwgch/sample_app/badge.png)](https://coveralls.io/r/kwgch/sample_app)
[![Dependency Status](https://gemnasium.com/kwgch/sample_app.png)](https://gemnasium.com/kwgch/sample_app)


# Ruby on Rails チュートリアル：サンプルアプリケーション

これは、以下のためのサンプルアプリケーションです。
[*Ruby on Rails Tutorial*](http://railstutorial.jp/)
by [Michael Hartl](http://michaelhartl.com/).
